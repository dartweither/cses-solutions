# cses solutions

Solutions for some of the cses.fi problems

## Subordinates (task1674)

Just start dfs from the general director and count number of childs in subtree as number of subtrees of childs + childs

## Tree Matching(task1130)

To count max number of pairs, count the number of pairs in a subtree without vertice we going in and with vertice
(1) dp1[v] = sum dp2[to]
(2) dp2[v] = max sum of subtree results if we use pair {v, to}; = dp1[v] - dp2[to] + dp1[to] + 1

## Tree Diameter(task1131)

Use 2 bfs, the first to find vertex with max distance from random vertex to it (this vertex should be one of the diameter ends), the second to find the most distant from found end of the diameter. 
