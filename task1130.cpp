#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> g;
vector<int> used, dp1, dp2;

void dfs(int v, int from)
{
	used[v] = 1;
	for (int to : g[v])
	{
		if (from == to)
		{
			continue;
		}
		if (!used[to])
		{
			dfs(to, v);
		}
		dp2[v] += dp1[to];
	}
	for (int to : g[v])
	{
		if (from == to)
		{
			continue;
		}
		if (!used[to])
		{
			dfs(to, v);
		}
		dp1[v] = max(dp1[v], dp2[v] - dp1[to] + dp2[to]);
	}
	dp1[v]++;
}

int main()
{
	int n;
	cin >> n;
	g.resize(n);
	used.assign(n, 0);
	dp1.assign(n, -1);
	dp2.assign(n, 0);
	for (int i = 0; i < n - 1; ++i)
	{
		int a, b;
		cin >> a >> b;
		a--;
		b--;
		g[a].push_back(b);
		g[b].push_back(a);
	}
	for (int i = 0; i < n; ++i)
	{
		if (!used[i])
		{
			dfs(i, -1);
		}
	}
	int res = 0;
	for (int i = 0; i < n; ++i)
	{
		res = max(res, max(dp1[i], dp2[i]));
	}
	cout << res;
	return 0;
}
