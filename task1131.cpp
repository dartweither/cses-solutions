#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> g;
vector<int> used, dist;

pair<int, int> bfs(int v) {
	queue<int> q;
	q.push(v);
	used[v] = 1;
	int w = v;
	while (!q.empty()) {
		int u = q.front(); q.pop();
		for (int to : g[u]) {
			if (!used[to]) {
				used[to] = 1;
				q.push(to);
				dist[to] = dist[u] + 1;
				if (dist[to] > dist[w]) w = to;
			}
		}
	}
	return { w, dist[w] };
}

int main() {
	int n;
	cin >> n;
	g.resize(n);
	used.assign(n, 0);
	dist.assign(n, 0);
	for (int i = 0; i < n - 1; ++i) {
		int a, b;
		cin >> a >> b;
		a--; b--;
		g[a].push_back(b);
		g[b].push_back(a);
	}
	auto[w, d] = bfs(0);
	used.assign(n, 0);
	dist.assign(n, 0);
	auto[v, res] = bfs(w);
	cout << res;
	return 0;
}
