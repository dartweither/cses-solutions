#include <bits/stdc++.h>
using namespace std;

vector<vector<int>> g;
vector<int> used, children;

void dfs(int v)
{
    used[v] = 1;
    for (int to : g[v])
    {
        if (!used[to])
        {
            dfs(to);
            children[v] += children[to] + 1;
        }
    }
}

int main()
{
    int n;
    cin >> n;
    g.resize(n);
    used.assign(n, 0);
    children.assign(n, 0);
    for (int i = 1; i < n; ++i)
    {
        int a;
        cin >> a;
        --a;
        g[a].push_back(i);
    }
    dfs(0);
    for (int i = 0; i < n; ++i)
    {
        cout << children[i] << " ";
    }
    return 0;
}